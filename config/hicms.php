<?php

return [

    'admin_prefix' => 'backend',

    'upload' => [
        'path' => storage_path('app/upload'),
        'maxsize' => 1024,
    ],
];
