<?php

namespace Hierarchy\Alexa\Console;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{

    public function boot()
    {
        # code...
    }

    public function register()
    {
        $this->commands([
            PublishThemeAssetsCommand::class,
        ]);
    }
}
