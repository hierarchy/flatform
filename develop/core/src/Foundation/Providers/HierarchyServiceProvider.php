<?php

namespace Hierarchy\Alexa\Foundation\Providers;

use Illuminate\Support\ServiceProvider;

class HierarchyServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // register package
        $this->registerPackage();
        $this->registerAliases();
    }

    /**
     * [registerPackage description]
     * @return [type] [description]
     */
    public function registerPackage()
    {
        $this->app->register(\Hierarchy\Alexa\Media\MediaServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Stylist\StylistServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Console\ConsoleServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Foundation\Theme\ThemeServiceProvider::class);
        $this->app->register(\Hierarchy\Alexa\Package\Providers\PackageServiceProvider::class);
    }

    public function registerAliases()
    {

    }
}
