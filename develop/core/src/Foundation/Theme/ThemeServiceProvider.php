<?php

namespace Hierarchy\Alexa\Foundation\Theme;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        // register stylist
        $this->app->booted(function () {
            $this->registerAllThemes();
            $this->setActiveTheme();
        });

        $this->app->singleton('theme.manager', function ($app) {
            $path = $app['config']->get('theme.path');
            return new ThemeManager($app, $path);
        });

        $loader = AliasLoader::getInstance();
        $loader->alias('ThemeManager', \Hierarchy\Alexa\Foundation\Theme\Facades\ThemeManager::class);
    }

    /**
     * Set the active theme based on the settings
     */
    private function setActiveTheme()
    {
        if ($this->app->runningInConsole()) {
            return;
        }

        if ($this->inAdministration()) {
            $themeName = $this->app['config']->get('theme.admin_theme');

            return $this->app['stylist']->activate($themeName, true);
        }

        $themeName = $this->app['config']->get('theme.frontend_theme', 'default');

        return $this->app['stylist']->activate($themeName, true);
    }

    /**
     * Check if we are in the administration
     * @return bool
     */
    private function inAdministration()
    {
        $segment = config('laravellocalization.hideDefaultLocaleInURL', false) ? 1 : 2;

        return $this->app['request']->segment($segment) === $this->app['config']->get('hicms.admin_prefix');
    }

    /**
     * Register all themes with activating them
     */
    private function registerAllThemes()
    {
        $directories = $this->app['files']->directories(config('stylist.themes.paths', [base_path('/themes')])[0]);

        foreach ($directories as $directory) {
            $this->app['stylist']->registerPath($directory);
        }
    }
}
