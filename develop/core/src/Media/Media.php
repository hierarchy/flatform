<?php

namespace Hierarchy\Alexa\Media;

use Hierarchy\Alexa\Media\Upload;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class Media
{
    protected $upload;
    protected $request;
    protected $image;

    public function __construct(ImageManager $image, Request $request, Upload $upload)
    {
        $this->image = $image;
        $this->request = $request;
        $this->upload = $upload;
    }

    public function upload($config = [])
    {
        $upload = $this->upload;

        if (array_key_exists('rules', $config)) {
            $upload->setRules($config['rules']);
        }
        if (array_key_exists('path', $config)) {
            $upload->setRules($config['path']);
        }
        if (array_key_exists('filename', $config)) {
            $upload->setRules($config['filename']);
        }
        if (array_key_exists('field', $config)) {
            $upload->setFieldName($config['field']);
        }
        return $upload->upload($this->request, true);
    }
}
