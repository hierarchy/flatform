<?php

namespace Hierarchy\Alexa\Media;

use Hierarchy\Alexa\Media\Media;
use Hierarchy\Alexa\Media\Upload;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageManager;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * [boot description]
     * @return [type] [description]
     */
    public function boot()
    {
        # code...
    }

    /**
     *
     */
    public function register()
    {
        $this->app->register(\Intervention\Image\ImageServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Image', \Intervention\Image\Facades\Image::class);

        $this->app->singleton('media', function ($app) {
            return new Media(new ImageManager, $app['request'], new Upload);
        });
    }
}
