<?php

namespace Hierarchy\Alexa\Media;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Upload
{

    protected $path;
    protected $errors;
    protected $filename;
    protected $rules = [];
    protected $info = [];
    protected $fieldName = 'file';

    public function __construct()
    {
        $this->filename = str_random(30);
        $this->path = config('hicms.upload.path');
    }

    public function upload(Request $request)
    {
        $file = $request->file($this->fieldName);

        if (!empty($this->rules) && !$this->validator($request, $this->rules)) {
            return $this->errors;
        }

        if ($file->isValid()) {
            $this->info['name'] = $file->getClientOriginalName();
            $this->info['file'] = $this->filename . '.' . $file->getClientOriginalExtension();
            $this->info['size'] = $file->getClientSize();
            $this->info['mime'] = $file->getClientMimeType();
            $this->info['ext'] = $file->getClientOriginalExtension();
            $this->info['path'] = $this->path;

            $file->move($this->path, $this->filename . '.' . $file->getClientOriginalExtension());
        }

        return $this->info;
    }

    public function validator($request, $rules)
    {
        if (!count($rules) > 0) {
            return true;
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->messages()->toArray();
            return false;
        }

        return true;
    }

    public function setRules($rules = [])
    {
        $this->rules = $rules;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path = null)
    {
        $this->path = $path;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }
}
