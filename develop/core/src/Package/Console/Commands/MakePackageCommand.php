<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\Package;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Process\Process;

class MakePackageCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new HiCMS Package and bootstrap it';

    /**
     * package folders to be created.
     *
     * @var array
     */
    protected $packageFolders = [
        // GUI folder struckture
        'Releases',
        'Releases/Master',
        'Releases/Master/Console/',
        'Releases/Master/Database/',
        'Releases/Master/Database/Migrations/',
        'Releases/Master/Database/Seeds/',
        'Releases/Master/Entities',
        'Releases/Master/Http/',
        'Releases/Master/Http/Controllers/',
        'Releases/Master/Http/Middleware/',
        'Releases/Master/Http/Requests/',
        'Releases/Master/Providers/',
        'Releases/Master/Repositories',
        'Releases/Master/Resources/',
        'Releases/Master/Resources/Assets/',
        'Releases/Master/Resources/Lang/',
        'Releases/Master/Resources/Views/',
    ];

    /**
     * package files to be created.
     *
     * @var array
     */
    protected $packageFiles = [
        // Gui file
        'Releases/Master/Database/Seeds/{{namespace}}DatabaseSeeder.php',
        'Releases/Master/Http/routes.php',
        'Releases/Master/Providers/{{namespace}}ServiceProvider.php',
        'Releases/Master/Providers/RouteServiceProvider.php',
        'Releases/Master/Resources/Views/welcome.blade.php',
        'Releases/Master/config.php',
        // manifest package
        'Releases/Master/package.json',
        // 'Releases/Master/config.php',
        'Releases/Master/Envoy.blade.php',
    ];

    /**
     * package stubs used to populate defined files.
     *
     * @var array
     */
    protected $packagetubs = [

        'seeder.stub',
        'routes.stub',
        'packageerviceprovider.stub',
        'routeserviceprovider.stub',
        'welcome.stub',
        'config.stub',

        // package stub
        'manifest.stub',
        // 'envoyconfig.stub',
        'envoy.stub',

    ];

    /**
     * The package instance.
     *
     * @var package
     */
    protected $package;

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Array to store the configuration details.
     *
     * @var array
     */
    protected $container;

    /**
     * Create a new command instance.
     *
     * @param Filesystem  $files
     * @param package  $package
     */
    public function __construct(Filesystem $files, Package $package)
    {
        parent::__construct();

        $this->files = $files;
        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->container['slug'] = strtolower($this->argument('slug'));
        $this->container['name'] = Str::studly($this->container['slug']);
        $this->container['namespace'] = Str::studly($this->container['slug']);

        $this->displayHeader('make_package_introduction');

        $this->stepOne();
    }

    /**
     * Step 1: Configure package manifest.
     *
     * @return mixed
     */
    private function stepOne()
    {
        $this->displayHeader('make_package_step_1');

        $this->container['name'] = $this->ask('Please enter the name of the package:', $this->container['name']);
        $this->container['slug'] = $this->ask('Please enter the slug for the package:', $this->container['slug']);
        $this->container['namespace'] = $this->ask('Please enter the namespace for the package:', $this->container['namespace']);
        $this->container['version'] = $this->ask('Please enter the package version:', '1.0');
        $this->container['description'] = $this->ask('Please enter the description of the package:', 'This is the description for the ' . $this->container['name'] . ' package.');
        $this->container['author'] = $this->ask('Please enter the author of the package:', ' ');
        $this->container['license'] = $this->ask('Please enter the package license:', 'MIT');

        $this->comment('You have provided the following manifest information:');
        $this->comment('Name:        ' . $this->container['name']);
        $this->comment('Slug:        ' . $this->container['slug']);
        $this->comment('Namespace:   ' . $this->container['namespace']);
        $this->comment('Version:     ' . $this->container['version']);
        $this->comment('Description: ' . $this->container['description']);
        $this->comment('Author:      ' . $this->container['author']);
        $this->comment('License:     ' . $this->container['license']);

        if ($this->confirm('Do you wish to continue?')) {
            $this->comment('Thanks! That\'s all we need.');
            $this->comment('Now relax while your package is generated for you.');

            $this->generate();
        } else {
            return $this->stepOne();
        }

        return true;
    }

    /**
     * Generate the package.
     */
    protected function generate()
    {
        $steps = [
            'Generating folders...' => 'generateFolders',
            'Generating .gitkeep...' => 'generateGitkeep',
            'Generating files...' => 'generateFiles',
            'Create Symlink Current' => 'createSymlink',
            'Resetting package cache...' => 'resetCache',
            // 'Optimizing Laravel...'     => 'optimizeLaravel'
        ];

        $progress = new ProgressBar($this->output, count($steps));
        $progress->start();

        foreach ($steps as $message => $function) {
            $progress->setMessage($message);

            $this->$function();

            $progress->advance();
        }

        $progress->finish();
        $this->info("\npackage generated successfully.");
    }

    /**
     * Generate defined package folders.
     *
     * @return void
     */
    protected function generateFolders()
    {
        if (!$this->files->isDirectory($this->package->getPath())) {
            $this->files->makeDirectory($this->package->getPath());
        }

        $this->files->makeDirectory($this->getPackagePath($this->container['slug'], true));

        foreach ($this->packageFolders as $folder) {
            $this->files->makeDirectory($this->getPackagePath($this->container['slug']) . $folder);
        }
    }

    /**
     * Generate defined package files.
     *
     * @return void
     */
    protected function generateFiles()
    {
        foreach ($this->packageFiles as $key => $file) {
            $file = $this->formatContent($file);

            $this->files->put($this->getDestinationFile($file), $this->getStubContent($key));
        }
    }

    /**
     * Generate .gitkeep files within generated folders.
     *
     * @return null
     */
    protected function generateGitkeep()
    {
        $packagePath = $this->getPackagePath($this->container['slug']);
        foreach ($this->packageFolders as $folder) {
            $gitkeep = $packagePath . $folder . '/.gitkeep';
            $this->files->put($gitkeep, '');
        }
    }

    /**
     * createSymlink Current
     * @return null
     */
    public function createSymlink()
    {
        $packagePath = $this->getPackagePath($this->container['slug']);

        $processgui = new Process("ln -nfs {$packagePath}/Releases/Master {$packagePath}/Current");
        $processgui->setTimeout(3600);
        $processgui->setIdleTimeout(300);
        $processgui->run();

        // $newprocess = new Process("chgrp -h www-data {$packagePath}/Current");
        // $process->setTimeout(3600);
        // $process->setIdleTimeout(300);
        // $process->run();
    }

    /**
     * Reset package cache of enabled and disabled package.
     *
     * @return void
     */
    protected function resetCache()
    {
        return $this->callSilent('package:cache');
    }

    /**
     * Optimize Laravel for better performance.
     *
     * @return void
     */
    protected function optimizeLaravel()
    {
        return $this->callSilent('optimize');
    }

    /**
     * Get the path to the package.
     *
     * @param  string $slug
     * @return string
     */
    protected function getPackagePath($slug = null, $allowNotExists = false)
    {
        if ($slug) {
            return $this->package->getPackagePath($slug, $allowNotExists);
        }

        return $this->package->getPath();
    }

    /**
     * Get destination file.
     *
     * @param  string $file
     * @return string
     */
    protected function getDestinationFile($file)
    {
        return $this->getPackagePath($this->container['slug']) . $this->formatContent($file);
    }

    /**
     * Get stub content by key.
     *
     * @param int  $key
     * @return string
     */
    protected function getStubContent($key)
    {
        return $this->formatContent($this->files->get(__DIR__ . '/../stubs/' . $this->packagetubs[$key]));
    }

    /**
     * Replace placeholder text with correct values.
     *
     * @return string
     */
    protected function formatContent($content)
    {
        return str_replace(
            ['{{slug}}', '{{name}}', '{{namespace}}', '{{version}}', '{{description}}', '{{author}}', '{{path}}', '{{realpath}}'],
            [$this->container['slug'], $this->container['name'], $this->container['namespace'], $this->container['version'], $this->container['description'], $this->container['author'], $this->package->getNamespace(), $this->package->getPath()],
            $content
        );
    }

    /**
     * Pull the given stub file contents and display them on screen.
     *
     * @param string  $file
     * @param string  $level
     * @return mixed
     */
    protected function displayHeader($file = '', $level = 'info')
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/console/' . $file . '.stub');
        return $this->$level($stub);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['slug', InputArgument::REQUIRED, 'The slug of the package'],
        ];
    }
}
