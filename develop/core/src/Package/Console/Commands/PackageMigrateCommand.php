<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\package;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PackageMigrateCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the database migrations for a specific or all Package';

    /**
     * @var package
     */
    protected $package;

    /**
     * @var Migrator
     */
    protected $migrator;

    /**
     * Create a new command instance.
     *
     * @param Migrator  $migrator
     * @param package  $package
     */
    public function __construct(Migrator $migrator, package $package)
    {
        parent::__construct();

        $this->migrator = $migrator;
        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (!$this->confirmToProceed()) {
            return null;
        }

        $this->prepareDatabase();

        $package = $this->package->getProperties($this->argument('slug'));

        if (!is_null($package)) {
            if ($this->package->isEnabled($package['slug'])) {
                return $this->migrate($package['slug']);
            } elseif ($this->option('force')) {
                return $this->migrate($package['slug']);
            }
        } else {
            if ($this->option('force')) {
                $Package = $this->package->all();
            } else {
                $Package = $this->package->enabled();
            }

            foreach ($Package as $package) {
                $this->migrate($package['slug']);
            }
        }
    }

    /**
     * Run migrations for the specified package.
     *
     * @param  string $slug
     * @return mixed
     */
    protected function migrate($slug)
    {
        if ($this->package->exists($slug)) {
            $pretend = $this->option('pretend');
            $path = $this->getMigrationPath($slug);

            $this->migrator->run($path, ['pretend' => $pretend]);

            // Once the migrator has run we will grab the note output and send it out to
            // the console screen, since the migrator itself functions without having
            // any instances of the OutputInterface contract passed into the class.
            foreach ($this->migrator->getNotes() as $note) {
                if (!$this->option('quiet')) {
                    $this->output->writeln($note);
                }
            }

            // Finally, if the "seed" option has been given, we will re-run the database
            // seed task to re-populate the database, which is convenient when adding
            // a migration and a seed at the same time, as it is only this command.
            if ($this->option('seed')) {
                $this->call('package:seed', ['package' => $slug, '--force' => true]);
            }
        } else {
            return $this->error("package does not exist.");
        }
    }

    /**
     * Get migration directory path.
     *
     * @param  string $slug
     * @return string
     */
    protected function getMigrationPath($slug)
    {
        $path = $this->package->getPackagePath($slug);

        return $path . 'Database/Migrations/';
    }

    /**
     * Prepare the migration database for running.
     *
     * @return void
     */
    protected function prepareDatabase()
    {
        $this->migrator->setConnection($this->option('database'));

        if (!$this->migrator->repositoryExists()) {
            $options = array('--database' => $this->option('database'));

            $this->call('migrate:install', $options);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to use.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
            ['pretend', null, InputOption::VALUE_NONE, 'Dump the SQL queries that would be run.'],
            ['seed', null, InputOption::VALUE_NONE, 'Indicates if the seed task should be re-run.'],
        ];
    }
}
