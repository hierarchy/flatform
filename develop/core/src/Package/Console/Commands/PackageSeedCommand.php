<?php

namespace Hierarchy\Alexa\Package\Console\Commands;

use Hierarchy\Alexa\Package\package;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PackageSeedCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'package:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records for a specific or all package';

    /**
     * @var package
     */
    protected $package;

    /**
     * Create a new command instance.
     *
     * @param package  $package
     */
    public function __construct(package $package)
    {
        parent::__construct();

        $this->package = $package;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $slug = $this->argument('slug');

        if (isset($slug)) {
            if (!$this->package->exists($slug)) {
                return $this->error("Module does not exist.");
            }

            if ($this->package->isEnabled($slug)) {
                $this->seed($slug);
            } elseif ($this->option('force')) {
                $this->seed($slug);
            }

            return;
        } else {
            if ($this->option('force')) {
                $Package = $this->package->all();
            } else {
                $Package = $this->package->enabled();
            }

            foreach ($Package as $package) {
                $this->seed($package['slug']);
            }
        }
    }

    /**
     * Seed the specific module.
     *
     * @param  string $package
     * @return array
     */
    protected function seed($slug)
    {
        $package = $this->package->getProperties($slug);
        $params = [];
        $namespacePath = $this->package->getNamespace();
        $rootSeeder = $package['namespace'] . 'DatabaseSeeder';
        $fullPath = $namespacePath . '\\' . $package['namespace'] . '\Database\Seeds\\' . $rootSeeder;

        if (class_exists($fullPath)) {
            if ($this->option('class')) {
                $params['--class'] = $this->option('class');
            } else {
                $params['--class'] = $fullPath;
            }

            if ($option = $this->option('database')) {
                $params['--database'] = $option;
            }

            if ($option = $this->option('force')) {
                $params['--force'] = $option;
            }

            $this->call('db:seed', $params);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [['slug', InputArgument::OPTIONAL, 'package slug.']];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['class', null, InputOption::VALUE_OPTIONAL, 'The class name of the module\'s root seeder.'],
            ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to seed.'],
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run while in production.'],
        ];
    }
}
