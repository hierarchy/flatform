<?php

namespace Hierarchy\Alexa\Package\Contracts;

/**
 * Contracts Repository
 */
interface RepositoryInterface
{
    /**
     * Get all package.
     *
     * @return Collection
     */
    public function all();

    /**
     * Get all package slugs.
     *
     * @return Collection
     */
    public function slugs();

    /**
     * Get Package based on where clause.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return Collection
     */
    public function where($key, $value);

    /**
     * Sort Package by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortBy($key);

    /**
     * Sort Package by given key in ascending order.
     *
     * @param  string  $key
     * @return Collection
     */
    public function sortByDesc($key);

    /**
     * Determines if the given package exists.
     *
     * @param  string  $slug
     * @return bool
     */
    public function exists($slug);

    /**
     * Returns a count of all package.
     *
     * @return int
     */
    public function count();

    /**
     * Returns the Package defined properties.
     *
     * @param  string  $slug
     * @return Collection
     */
    public function getProperties($slug);

    /**
     * Returns the given package property.
     *
     * @param  string       $property
     * @param  mixed|null   $default
     * @return mixed|null
     */
    public function getProperty($property, $default = null);

    /**
     * Set the given package property value.
     *
     * @param  string  $property
     * @param  mixed   $value
     * @return bool
     */
    public function setProperty($property, $value);

    /**
     * Get all enabled package.
     *
     * @return Collection
     */
    public function enabled();

    /**
     * Get all disabled package.
     *
     * @return Collection
     */
    public function disabled();

    /**
     * Determines if the specified package is enabled.
     *
     * @param  string  $slug
     * @return bool
     */
    public function isEnabled($slug);

    /**
     * Determines if the specified package is disabled.
     *
     * @param  string  $slug
     * @return bool
     */
    public function isDisabled($slug);

    /**
     * Enables the specified package.
     *
     * @param  string  $slug
     * @return bool
     */
    public function enable($slug);

    /**
     * Disables the specified package.
     *
     * @param  string  $slug
     * @return bool
     */
    public function disable($slug);

    /**
     * Refresh the cache with any newly found package.
     *
     * @return bool
     */
    public function cache();

    /**
     * Get the contents of the cache file.
     *
     * The cache file lists all package slugs and their
     * enabled or disabled status. This can be used to
     * filter out Package depending on their status.
     *
     * @return Collection
     */
    public function getCache();

    /**
     * Set the given cache key value.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return int
     */
    public function setCache($key, $value);
}
