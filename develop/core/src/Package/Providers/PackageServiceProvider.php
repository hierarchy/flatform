<?php

namespace Hierarchy\Alexa\Package\Providers;

use Illuminate\Support\ServiceProvider;

/**
 *
 */
class PackageServiceProvider extends ServiceProvider
{
    /**
     * Boot the HiSoft services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // register Package
        $this->app['package']->register();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register('Hierarchy\Alexa\Package\Providers\ConsoleServiceProvider');
        $this->app->register('Hierarchy\Alexa\Package\Providers\GeneratorServiceProvider');
        $this->app->register('Hierarchy\Alexa\Package\Providers\RepositoryServiceProvider');

        $this->app->singleton('package', function ($app) {
            $repository = $app->make('Hierarchy\Alexa\Package\Contracts\RepositoryInterface');
            return new \Hierarchy\Alexa\Package\Package($app, $repository);
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return string
     */
    public function provides()
    {
        return ['package'];
    }
}
