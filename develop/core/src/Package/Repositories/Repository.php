<?php

namespace Hierarchy\Alexa\Package\Repositories;

use Hierarchy\Alexa\Package\Contracts\RepositoryInterface;
use Illuminate\Filesystem\Filesystem;

abstract class Repository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * @var string $path Path to the defined Package directory
     */
    protected $path;

    /**
     * Constructor method.
     *
     * @param \Illuminate\Filesystem\Filesystem  $files
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * Get all package basenames
     *
     * @return array
     */
    protected function getAllBasenames()
    {
        $path = $this->getPath();

        try {
            $collection = collect($this->files->directories($path));

            $basenames = $collection->map(function ($item, $key) {
                return basename($item);
            });

            return $basenames;
        } catch (\InvalidArgumentException $e) {
            return collect(array());
        }
    }

    /**
     * Get Package path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path ?: config('package.path');
    }

    /**
     * Set Package path in "RunTime" mode.
     *
     * @param  string $path
     * @return object $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path for the specified package.
     *
     * @param  string $slug
     * @return string
     */
    public function getPackagePath($slug)
    {
        $package = studly_case($slug);

        return $this->getPath() . "/{$package}/";
    }

    /**
     * Get path of package manifest file.
     *
     * @param  string $package
     * @return string
     */
    protected function getManifestPath($slug)
    {
        return $this->getPackagePath($slug) . 'Current/package.json';
    }

    /**
     * Get Package namespace.
     *
     * @return string
     */
    public function getNamespace()
    {
        return rtrim(config('package.namespace'), '/\\');
    }
}
