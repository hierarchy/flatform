<?php

namespace Hierarchy\Alexa\Package\Traits;

trait MigrationTrait
{
    /**
     * Require (once) all migration files for the supplied package.
     *
     * @param  string $package
     * @return void
     */
    protected function requireMigrations($package)
    {
        $path = $this->getMigrationPath($package);

        $migrations = $this->laravel['files']->glob($path . '*_*.php');

        foreach ($migrations as $migration) {
            $this->laravel['files']->requireOnce($migration);
        }
    }

    /**
     * Get migration directory path.
     *
     * @param  string $package
     * @return string
     */
    protected function getMigrationPath($package)
    {
        $path = $this->laravel['package']->getPackagePath($package);

        return $path . 'Database/Migrations/';
    }
}
