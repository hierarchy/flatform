<?php

use Illuminate\Contracts\View\Factory as ViewFactory;

/**
 * Helpers hierarchy
 */
if (!function_exists('theme')) {

    function theme($view = null, $data = [], $mergeData = [])
    {
        $factory = app(ViewFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($view, $data, $mergeData);
    }
}
